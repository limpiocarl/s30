const express = require("express");
const mongoose = require("mongoose");

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Syntax:
// mongoose.connect('<connection string>', {middlewares})

mongoose.connect(
  "mongodb+srv://admin:admin131@zuittbootcamp.0e0ak.mongodb.net/session30?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

//console.error.bind(console)- print errors in the browser and in the terminal
db.on("error", console.error.bind(console, "Connection Error"));

// if the connection is successful, this will be the output in our console.
db.once("open", () => console.log("Connected to the cloud database"));

// Mongoose Schema

const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: "pending",
  },
});

const Task = mongoose.model("Task", taskSchema);
// models use schemas and they act as the middleman from the server to our database
// model can now be uses to run commands for interacting with our database.
// naming convention - name of model should be capitalized and singular form "Task"
// second parameter is used to specify the schema of the documents that will be stored in the mongoDB collection

// Business Logic

/* 
1. Add a functionality to check if there are duplicate tasks
- if the task



*/

app.post("/tasks", (req, res) => {
  // check any duplicate task
  // err shorthand of error
  Task.findOne({ name: req.body.name }, (err, result) => {
    // if there are matches
    if (result != null && result.name === req.body.name) {
      // will return this message
      return res.send("Duplicate task found.");
    } else {
      let newTask = new Task({
        name: req.body.name,
      });
      // save method will accept a callback function which stores any errors in the first parameter and will store the newly saved document in the second parameter
      newTask.save((saveErr, savedTask) => {
        if (saveErr) {
          // if there are any errors, it will print the error.
          return console.error(saveErr);
        }
        // if there are no error found, it will return this message
        else {
          return res.status(201).send("New Task Created");
        }
      });
    }
  });
});

app.get("/tasks", (req, res) => {
  Task.find({}, (err, result) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});

// Activity

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

// register user if no duplicate username
const User = mongoose.model("User", userSchema);
app.post("/signup", (req, res) => {
  User.findOne({ username: req.body.username }, (err, result) => {
    // if there are matches
    if (result != null && result.username === req.body.username) {
      // will return this message
      return res.send("Username is already taken.");
    } else {
      let register = new User({
        username: req.body.username,
        password: req.body.password,
      });
      register.save((err, result) => {
        if (err) {
          return console.log(err);
        } else {
          return res.status(201).send("You are now registered.");
        }
      });
    }
  });
});

// retrieve all users
app.get("/users", (req, res) => {
  User.find({}, (err, result) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});

app.listen(port, () => console.log(`Server is running at port ${port}`));
